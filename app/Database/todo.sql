DROP DATABASE if EXISTS todo;

CREATE DATABASE todo;

use todo;

create table user (
    id int PRIMARY KEY auto_increment,
    username varchar(30) not null UNIQUE,
    password varchar(255) not null,
    firstname varchar(100),
    lastname varchar(100)
);

create table task(
    id int PRIMARY KEY AUTO_INCREMENT,
    title varchar(255) not null,
    added timestamp DEFAULT CURRENT_TIMESTAMP,
    description text,
    user_id int not null,
    index (user_id),
    foreign key (user_id) references user(id)
    on delete restrict
    )