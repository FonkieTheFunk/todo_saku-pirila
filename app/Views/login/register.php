<h3><?= $title ?> </h3>
<form action="/register/check">
<div class="col-12">
<?= \Config\Services::validation()->listErrors();?>
</div>
<div class="form-group">
    <label>Username</label>
    <input class="form-control" name="user" placeholder="Enter username" maxlength="30">
</div>
<div class="form-group">
    <label>First name</label>
    <input class="form-control" name="firstname" placeholder="Enter First name" maxlength="30">
</div>
<div class="form-group">
    <label>Last name</label>
    <input class="form-control" name="Lastname" placeholder="Last name" maxlength="30">
</div>
<div class="form-group">
<label>Password</label>
<input class="form-control" name="password" type="password" placeholder="Enter Password" maxlength="30">
</div>
<button class="btn btn-primary">Register</button>
<?= anchor('login/register','Register')?>
</form>