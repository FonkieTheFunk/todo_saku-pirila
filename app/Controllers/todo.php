<?php namespace App\controllers;

use app\Models\TodoModel;



class todo extends BaseController{
    
    
    public function index(){
        $model = new TodoModel();
        $data["title"] = "todo";
        $data["todos"] = $model->getTodos();
        echo view("templates/header", $data);
        echo view("todo/list", $data);
        echo view("templates/footer", $data);
    }

    public function __construct()
    {
        $_SESSION = \config\Services :: session();
        $_SESSION->start();
    }
    public function create() {
        $model = new TodoModel();

        if (!$this->validate([
            'title' => 'required|max_length[255]',
        ])){
            echo view("templates/header", ["title" => "add new task"]);
            echo view("tood/create");
            echo view("templates/footer");
        }
        else {
            $user = $_SESSION["user"];
            $model->save([
                "title" => $this->request->getVar('title'),
                "description" => $this->request->getVar("description"),
                "user_id" => $user->id
            ]);
            return redirect("todo");
        }
    }
    public function delete($id) {
        if (!is_numeric($id)){
            throw new \Exception("Provided id is not an number.");
        }
        if (!isset($_SESSION["user"])){
            return redirect("login");
        }
        $model = new TodoModel()

        $model->remove($id);
        return redirect("todo");
    }
}
